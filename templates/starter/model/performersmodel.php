<?php

class PerformersModel extends SuperModel {

	public function get_performers() {
		$table = CURRENT_LANG.'_performers';
		$params = [
			'where' => 'deleted = 0'
		];
		return $this->add_categories($this->get_arrays($table, $params));
	}

	private function add_categories($performers) {
		$controller = Application::get_class('CategoriesController');
		$categories = $controller->get_categories();
		$categories = call_user_func(function() use ($categories) {
			$result = [];
			foreach($categories as $el) {
				$result[$el['id']] = $el;
			}
			return $result;
		});
		foreach($performers as &$el) {
			$category = $categories[$el['category']];
			if(!is_array($category)) {
				$category = [];
			}
			$el['category'] = empty($category['name']) ? '' : $category['name'];
		}
		return $performers;
	}

	public function add_performer($fields) {
		$table = CURRENT_LANG.'_performers';
		return $this->insert($table, [
			'fields' => $fields
		]);
	}

	public function get_performer($id) {
		$table = CURRENT_LANG.'_performers';
		return $this->select($table, [
			'where' => 'id = '.((int)$id).' and deleted = 0'
		]);
	}

	public function delete_performer($id) {
		$table = CURRENT_LANG.'_performers';
		$this->update($table, [
			'fields' => [
				'deleted' => 1
			],
			'where' => 'id = '.((int)$id)
		]);
	}

	public function edit_performer($id, $fields) {
		$table = CURRENT_LANG.'_performers';
		$this->update($table, [
			'fields' => $fields,
			'where' => 'id = '.((int)$id)
		]);
	}
}