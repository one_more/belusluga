<?php

class CategoriesModel {
	use trait_json;
	private $data;
	private $data_file;

	public function __construct() {
		$template = Application::get_class('Starter');
		$this->data_file = $template->path.DS.'lang'.DS.CURRENT_LANG.DS.'categories.json';
		$this->data = file_get_contents($this->data_file);
		$this->data = json_decode($this->data, true);
	}

	public function get_categories() {
		return array_filter($this->data, function($el) {
			return empty($el['deleted']) || $el['deleted'] == 0;
		});
	}

	public function get_category($id) {
		$result = array_filter($this->data, function($el) use($id) {
			return $el['id'] == $id;
		});
		return end($result);
	}

	public function category_exists($name) {
		$name = strtolower($name);
		foreach($this->data as $el) {
			if($name == strtolower($el['name'])) {
				return true;
			}
		}
		return false;
	}
	public function add_category($name) {
		$last = end($this->data);
		reset($this->data);
		$this->data[] = [
			'id' => $last['id']+1,
			'name' => $name
		];
		$this->save_data();
		return $last['id']+1;
	}

	public function update_category($id, $name) {
		$id = (int)$id;
		foreach($this->data as &$el) {
			if($el['id'] == $id) {
				$el['name'] = strval($name);
			}
		}
		$this->save_data();
	}

	public function delete_category($id) {
		$id = (int)$id;
		foreach($this->data as &$el) {
			if($el['id'] == $id) {
				$el['deleted'] = 1;
			}
		}
		$this->save_data();
	}

	private function save_data() {
		file_put_contents($this->data_file, $this->array_to_json_string($this->data));
	}
}