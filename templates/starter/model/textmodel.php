<?php

class TextModel {
	use trait_json;
	private $data;
	private $data_file;

	public function __construct() {
		$template = Application::get_class('Starter');
		$this->data_file = $template->path.DS.'lang'.DS.CURRENT_LANG.DS.'text.json';
		$this->data = file_get_contents($this->data_file);
		$this->data = json_decode($this->data, true);
	}

	public function get_section($section) {
		return empty($this->data[$section]) ? '' : $this->data[$section];
	}

	public function update_section($section, $text) {
		$this->data[$section] = $text;
		$this->save_data();
	}

	private function save_data() {
		file_put_contents($this->data_file, $this->array_to_json_string($this->data));
	}
}