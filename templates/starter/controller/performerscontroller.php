<?php

class PerformersController {
	use trait_controller, trait_validator;

	private $model;

	private $images_dir;

	public function __construct() {
		$this->model = $this->get_model('PerformersModel');
		$this->images_dir = ROOT_PATH.DS.'www'.DS.'starter'.DS.'images'.DS.'performers_images';
	}

	public function get_performers() {
		return array_map(function($el) {
			return $this->add_performer_photos($el);
		}, $this->model->get_performers());
	}

	public function add_performer() {
		$controller = Application::get_class('UserController');
		if(!$controller->is_admin()) {
			throw new Exception('you should be an admin to add new performer');
		}
		$lang_vars = $this->get_lang_vars();
		try {
			$fields = $this->get_sanitized_vars([
				[
					'name' => 'name',
					'required' => true,
					'error' => $lang_vars['add_performer']['enter_name'],
					'type' => 'string'
				],
				[
					'name' => 'phone',
					'required' => true,
					'error' => $lang_vars['add_performer']['enter_phone'],
					'type' => 'string'
				],
				[
					'name' => 'category',
					'type' => 'int',
					'required' => true,
					'error' => $lang_vars['add_performer']['enter_category']
				],
				[
					'name' => 'about',
					'type' => 'string',
					'required' => true,
					'error' => $lang_vars['add_performer']['enter_about']
				],
				[
					'name' => 'photos',
					'required' => true,
					'type' => 'string',
					'error' => $lang_vars['add_performer']['add_photos']
				]
			]);
		} catch(Exception $e) {
			die(json_encode([
				'status' => 'error',
				'message' => $e->getMessage()
			]));
		}
		unset($fields['photos']);
		echo json_encode([
			'status' => 'success',
			'message' => $lang_vars['add_performer']['success'],
			'id' => $this->model->add_performer($fields)
		]);
	}

	public function edit_performer() {
		$controller = Application::get_class('UserController');
		if(!$controller->is_admin()) {
			throw new Exception('you should be an admin to add new performer');
		}
		$lang_vars = $this->get_lang_vars();
		try {
			$fields = $this->get_sanitized_vars([
				[
					'name' => 'name',
					'required' => true,
					'error' => $lang_vars['add_performer']['enter_name'],
					'type' => 'string'
				],
				[
					'name' => 'phone',
					'required' => true,
					'error' => $lang_vars['add_performer']['enter_phone'],
					'type' => 'string'
				],
				[
					'name' => 'category',
					'type' => 'int',
					'required' => true,
					'error' => $lang_vars['add_performer']['enter_category']
				],
				[
					'name' => 'about',
					'type' => 'string',
					'required' => true,
					'error' => $lang_vars['add_performer']['enter_about']
				]
			]);
		} catch(Exception $e) {
			die(json_encode([
				'status' => 'error',
				'message' => $e->getMessage()
			]));
		}
		unset($fields['photos']);
		$id = Request::get_var('id', 'int');
		if(!$id) {
			throw new Exception('empty id in edit performer method');
		}
		$this->model->edit_performer($id, $fields);
		echo json_encode([
			'status' => 'success',
			'message' => $lang_vars['edit_performer']['success']
		]);
	}

	public function add_performer_images($id) {
		$controller = Application::get_class('UserController');
		if(!$controller->is_admin()) {
			throw new Exception('you should be an admin to add performer images');
		}
		$id = (int)$id;
		$dir = $this->images_dir.DS.$id;
		if(!is_dir($dir)) {
			mkdir($dir, 0777, $recursive = true);
		}
		if(!empty($_FILES['images']['name'][0])) {
			for($i=0; $i<count($_FILES['images']['name']);$i++) {
				$tmp_file = $_FILES['images']['tmp_name'][$i];
				list($type,$extension) = explode('/', $_FILES['images']['type'][$i]);
				if($type != 'image') {
					continue;
				}
				if(file_exists($tmp_file)) {
					$tmp_file_hash = md5(file_get_contents($tmp_file));
					$new_file = $dir.DS."{$tmp_file_hash}.{$extension}";
					if(!move_uploaded_file($tmp_file, $new_file)) {
						$message = 'could not upload file ';
						$message .= $tmp_file;
						throw new Exception($message);
					}
					@chmod($new_file, 0777);
				}
			}
		}
		echo json_encode([
			'status' => 'success'
		]);
	}

	public function get_performer($id) {
		return $this->add_performer_photos($this->model->get_performer($id));
	}

	private function add_performer_photos($performer) {
		$dir = $this->images_dir.DS.$performer['id'].DS.'*';
		$images = glob($dir);
		$performer['images'] = array_map(function($el) {
			return str_replace(ROOT_PATH.DS.'www', '', $el);
		}, $images);
		return $performer;
	}

	public function delete_performer($id) {
		$controller = Application::get_class('UserController');
		if(!$controller->is_admin()) {
			throw new Exception('you should be an admin to delete performer');
		}
		$this->model->delete_performer($id);
		$lang_vars = $this->get_lang_vars();
		$this->delete_performer_images($id);
		echo json_encode([
			'status' => 'success',
			'message' => $lang_vars['delete_performer']['success']
		]);
	}

	public function delete_performer_images($id, $images = null) {
		$dir = $this->images_dir.DS.$id;
		if(!$images) {
			if(is_dir($dir)) {
				Application::remove_dir($dir);
			}
		} else {
			foreach($images as $el) {
				$file = $dir.DS.$el;
				if(file_exists($file)) {
					unlink($file);
				}
			}
		}
	}
}