<?php

class CategoriesController {
	use trait_controller;

	private $model;

	public function __construct() {
		$this->model = Application::get_class('CategoriesModel');
	}

	public function get_categories() {
		return $this->model->get_categories();
	}

	public function get_category($id) {
		return $this->model->get_category($id);
	}

	public function add_category() {
		$user_controller = Application::get_class('UserController');
		if(!$user_controller->is_admin()) {
			throw new Exception('you must be admin to add category');
		}
		$name = Request::get_var('name', 'string');
		$lang_vars = $this->get_lang_vars();
		if(!$name) {
			echo json_encode([
				'status' => 'error',
				'message' => $lang_vars['add_category']['enter_name']
			]);
		} elseif($this->model->category_exists($name)) {
			echo json_encode([
				'status' => 'error',
				'message' => $lang_vars['add_category']['category_exists']
			]);
		} else {
			$this->model->add_category($name);
			echo json_encode([
				'status' => 'success',
				'message' => $lang_vars['add_category']['success']
			]);
		}
	}

	public function edit_category() {
		$user_controller = Application::get_class('UserController');
		if(!$user_controller->is_admin()) {
			throw new Exception('you must be admin to add category');
		}
		$id = Request::get_var('id', 'int');
		if(!$id) {
			throw new Exception('empty id');
		}
		$old_name = Request::get_var('old-name', 'string');
		$name = Request::get_var('name', 'string');
		$lang_vars = $this->get_lang_vars();
		if(strtolower($old_name) !== $name) {
			if($this->model->category_exists($name)) {
				echo json_encode([
					'status' => 'error',
					'message' => $lang_vars['add_category']['category_exists']
				]);
			} else {
				$this->model->update_category($id, $name);
				echo json_encode([
					'status' => 'success',
					'message' => $lang_vars['edit_category']['success']
				]);
			}
		} else {
			echo json_encode([
				'status' => 'success',
				'message' => $lang_vars['edit_category']['success']
			]);
		}
	}

	public function delete_category($id) {
		$user_controller = Application::get_class('UserController');
		if(!$user_controller->is_admin()) {
			throw new Exception('you must be admin to delete category');
		}
		$this->model->delete_category($id);
		$lang_vars = $this->get_lang_vars();
		echo json_encode([
			'status' => 'success',
			'message' => $lang_vars['delete_category']['success']
		]);
	}
}