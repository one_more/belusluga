<?php

class TextController {
	use trait_controller;

	private $model;

	public function __construct() {
		$this->model = Application::get_class('TextModel');
	}

	public function get_section($section) {
		return $this->model->get_section($section);
	}

	public function update_section($section, $text) {
		$this->model->update_section($section, $text);
	}
}