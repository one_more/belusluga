<?php

class SiteRouter extends Router {
	use trait_controller, trait_starter_router;

	private $positions = [
		'top_menu' => null,
		'left_menu' => null,
		'main_content' => null,
		'page_text' => null,
	];

	public function __construct() {
		$this->routes = [
			'/' => [$this, 'index', 'no check'],
			'/login' => [$this, 'login'],
			'/logout' => [$this, 'logout'],
			'/edit_user' => ['UserController', 'edit_user'],
			'/add_user' => ['UserController', 'add_user'],
			'/add_performer' => ['PerformersController', 'add_performer'],
			'/add_performer_images/:number' => ['PerformersController', 'add_performer_images'],
			'/delete_performer/:number' => ['PerformersController', 'delete_performer'],
			'/edit_performer' => ['PerformersController', 'edit_performer'],
			'/delete_performer_images' => [$this, 'delete_performer_images'],
			'/add_category' => ['CategoriesController', 'add_category'],
			'/delete_category/:number' => ['CategoriesController', 'delete_category'],
			'/edit_category' => ['CategoriesController', 'edit_category'],
			'/language_model' => [$this, 'language_model', 'no check'],
			'/update_text' => [$this, 'update_text'],
			'/goods' => [$this, 'goods', 'no check']
		];
	}

	public function index() {
		$view = Application::get_class('Services');
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}

	public function login() {
		$login = Request::get_var('login', 'string');
		$password = Request::get_var('password', 'string');
		$remember = Request::get_var('remember', 'string');
		$user_controller = Application::get_class('UserController');
		echo json_encode($user_controller->login($login, $password, (bool)$remember));
	}

	public function language_model() {
		$template = Application::get_class('Starter');
		echo file_get_contents($template->path.DS.'lang'.DS.CURRENT_LANG.DS.'client.json');
	}

	public function logout() {
		$user = Application::get_class('User');
		$user->log_out();
	}

	public function delete_performer_images() {
		$user_controller = Application::get_class('UserController');
		if(!$user_controller->is_admin()) {
			throw new Exception('you must be admin to delete performer images');
		}
		$controller = Application::get_class('PerformersController');
		$id = Request::get_var('id', 'int');
		$images = Request::get_var('images', 'raw');
		$controller->delete_performer_images($id, $images);
	}

	public function update_text() {
		$user_controller = Application::get_class('UserController');
		if($user_controller->is_admin()) {
			require_once(ROOT_PATH.DS.'lib'.DS.'HTMLPurifier'.DS.'HTMLPurifier.auto.php');
			$controller = Application::get_class('TextController');
			$section = Request::get_var('section', 'string');
			$text = trim(Request::get_var('text'));
			$config = HTMLPurifier_Config::createDefault();
			$purifier = new HTMLPurifier($config);
			$text = $purifier->purify($text);
			$controller->update_section($section, $text);
		}
	}

	public function goods() {
		$this->positions['main_content'] = 'Товаров пока нет';
		$this->show_result();
	}

	private function show_result() {
		if(!Request::is_ajax()) {
			$template   = Application::get_class('Starter');
			$templator = new Smarty();
			$static_path = DS.'starter';
			$static_paths = [
				'css_path' => $static_path.DS.'css',
				'images_path' => $static_path.DS.'images',
				'js_path' => $static_path.DS.'js'
			];
			$top_menu_view = Application::get_class('MainMenu');
			$this->positions['top_menu'] = $top_menu_view->render();
			$templator->assign($static_paths);
			if($this->positions['page_text'] == null) {
				$view = Application::get_class('Text', ['main_page']);
				$this->positions['page_text'] = $view->render();
			}
			$templator->setTemplateDir($template->path.DS.'templates'.DS.'index');
			$templator->setCompileDir($template->path.DS.'templates_c');
			$templator->assign($this->positions);
			echo $templator->getTemplate('index.tpl.html');
		} else {
			$this->positions = array_filter($this->positions, function($el) {
				return $el !== null;
			});
			echo json_encode($this->positions);
		}
	}
}