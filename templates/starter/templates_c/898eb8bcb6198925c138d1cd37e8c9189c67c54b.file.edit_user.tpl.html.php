<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-25 22:57:19
         compiled from "/var/www/belusluga.my/templates/starter/templates/admin_panel/edit_user.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:1458804696553bf19fe79a77-64112969%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '898eb8bcb6198925c138d1cd37e8c9189c67c54b' => 
    array (
      0 => '/var/www/belusluga.my/templates/starter/templates/admin_panel/edit_user.tpl.html',
      1 => 1429954312,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1458804696553bf19fe79a77-64112969',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user' => 0,
    'login' => 0,
    'password' => 0,
    'credentials' => 0,
    'credentials_labels' => 0,
    'k' => 0,
    'el' => 0,
    'save' => 0,
    'cancel' => 0,
    'user_not_exists' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_553bf19fee7441_61859156',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553bf19fee7441_61859156')) {function content_553bf19fee7441_61859156($_smarty_tpl) {?><?php if (count($_smarty_tpl->tpl_vars['user']->value)) {?>
    <form id="edit-user-form">
        <input name="id" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
"/>
        <div class="row">
            <div class="col-lg-5">
                <div class="form-group">
                    <label for="login"><?php echo $_smarty_tpl->tpl_vars['login']->value;?>
</label>
                    <input value="<?php echo $_smarty_tpl->tpl_vars['user']->value['login'];?>
" id="login" name="login" class="form-control" type="text"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <div class="form-group">
                    <label for="password"><?php echo $_smarty_tpl->tpl_vars['password']->value;?>
</label>
                    <input value="<?php echo $_smarty_tpl->tpl_vars['user']->value['password'];?>
" class="form-control" id="password" name="password" type="text"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <div class="form-group">
                    <label for="credentials"><?php echo $_smarty_tpl->tpl_vars['credentials']->value;?>
</label>
                    <select name="credentials" id="credentials" class="form-control">
                        <?php  $_smarty_tpl->tpl_vars['el'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['el']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['credentials_labels']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['el']->key => $_smarty_tpl->tpl_vars['el']->value) {
$_smarty_tpl->tpl_vars['el']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['el']->key;
?>
                        <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['user']->value['credentials']) {?>
                        <option selected value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['el']->value;?>
</option>
                        <?php } else { ?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['el']->value;?>
</option>
                        <?php }?>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <div class="form-group">
                    <input class="btn btn-success" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['save']->value;?>
"/>
                    <button class="btn btn-info"><?php echo $_smarty_tpl->tpl_vars['cancel']->value;?>
</button>
                </div>
            </div>
        </div>
    </form>
    <?php } else { ?>
        <div class="alert alert-danger"><?php echo $_smarty_tpl->tpl_vars['user_not_exists']->value;?>
</div>
<?php }?>
<?php }} ?>
