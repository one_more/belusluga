<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-26 14:34:34
         compiled from "/var/www/belusluga.my/templates/starter/templates/admin_panel/add_performer.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:539320636553c04ccae62a5-59754648%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7906a1fab6215de0773d725d45a944794e2de4e3' => 
    array (
      0 => '/var/www/belusluga.my/templates/starter/templates/admin_panel/add_performer.tpl.html',
      1 => 1430048025,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '539320636553c04ccae62a5-59754648',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_553c04ccb857e7_60639282',
  'variables' => 
  array (
    'data' => 0,
    'photos' => 0,
    'name' => 0,
    'phone' => 0,
    'category' => 0,
    'categories' => 0,
    'el' => 0,
    'about' => 0,
    'select_photo' => 0,
    'add' => 0,
    'cancel' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553c04ccb857e7_60639282')) {function content_553c04ccb857e7_60639282($_smarty_tpl) {?><form id="add-performer-form">
    <ul id="tabs" class="nav nav-tabs">
        <li class="active"><a class="cursor-pointer" data-param="data"><?php echo $_smarty_tpl->tpl_vars['data']->value;?>
</a></li>
        <li><a class="cursor-pointer" data-param="photo"><?php echo $_smarty_tpl->tpl_vars['photos']->value;?>
</a></li>
    </ul>
    <div id="tabs-data">
        <div id="data">
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="name"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</label>
                        <input id="name" name="name" class="form-control" type="text"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="phone"><?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
</label>
                        <input class="form-control" id="phone" name="phone" type="text"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="category"><?php echo $_smarty_tpl->tpl_vars['category']->value;?>
</label>
                        <select name="category" id="category" class="form-control">
                            <?php  $_smarty_tpl->tpl_vars['el'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['el']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['el']->key => $_smarty_tpl->tpl_vars['el']->value) {
$_smarty_tpl->tpl_vars['el']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['el']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['el']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['el']->value['name'];?>
</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="about"><?php echo $_smarty_tpl->tpl_vars['about']->value;?>
</label>
                        <textarea class="form-control" id="about" name="about"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div id="photo" class="hide margin-top-10px margin-bottom-10px">
            <div class="row">
                <div class="col-lg-4 padding-right-5px">
                    <input name="photos" readonly class="form-control" id="photos-names" type="text"/>
                </div>
                <div class="col-lg-4 padding-0">
                    <button class="btn btn-primary" id="select-photo"><?php echo $_smarty_tpl->tpl_vars['select_photo']->value;?>
</button>
                </div>
                <input name="images" type="file" multiple class="opacity-0"/>
            </div>
            <div class="row">
                <div id="thumbnails">

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <input class="btn btn-success" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['add']->value;?>
"/>
                <button id="cancel" class="btn btn-info"><?php echo $_smarty_tpl->tpl_vars['cancel']->value;?>
</button>
            </div>
        </div>
    </div>
</form>
<?php }} ?>
