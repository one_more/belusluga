<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-05-01 01:00:49
         compiled from "/var/www/belusluga.my/templates/starter/templates/services/services.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:724708806553fd268074b76-60301648%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '19d30d8243106a929a536bee1393e049729ea99d' => 
    array (
      0 => '/var/www/belusluga.my/templates/starter/templates/services/services.tpl.html',
      1 => 1430431247,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '724708806553fd268074b76-60301648',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_553fd26809e5b9_40593567',
  'variables' => 
  array (
    'performers' => 0,
    'performer' => 0,
    'image' => 0,
    'of' => 0,
    'view_full_size' => 0,
    'prev' => 0,
    'next' => 0,
    'close' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553fd26809e5b9_40593567')) {function content_553fd26809e5b9_40593567($_smarty_tpl) {?><div id="performers" class="row">
    <?php  $_smarty_tpl->tpl_vars['performer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['performer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['performers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['performer']->key => $_smarty_tpl->tpl_vars['performer']->value) {
$_smarty_tpl->tpl_vars['performer']->_loop = true;
?>
        <div class="col-lg-6 col-xs-10 col-sm-6 col-md-6 margin-bottom-20px">
            <div class="performer">
                <div class="preview">
                    <div class="performer-images swiper-container">
                        <div class="swiper-wrapper">
                            <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['performer']->value['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
                                <div class="swiper-slide">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" alt=""/>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="category"><?php echo $_smarty_tpl->tpl_vars['performer']->value['category'];?>
</div>
                </div>
                <div class="performer-info hide">
                    <p class="text-right">
                        <span class="slides-active-index">1</span>
                        <?php echo $_smarty_tpl->tpl_vars['of']->value;?>

                        <?php echo count($_smarty_tpl->tpl_vars['performer']->value['images']);?>

                    </p>
                    <p class="text-bold text-center">
                        <span class="pull-left cursor-pointer open-slideshow"><?php echo $_smarty_tpl->tpl_vars['view_full_size']->value;?>
</span>
                        <span>
                            <span class="cursor-pointer prev-slide"><?php echo $_smarty_tpl->tpl_vars['prev']->value;?>
</span>
                            -
                            <span class="cursor-pointer next-slide"><?php echo $_smarty_tpl->tpl_vars['next']->value;?>
</span>
                        </span>
                        <span class="pull-right hide-info cursor-pointer"><?php echo $_smarty_tpl->tpl_vars['close']->value;?>
</span>
                    </p>
                    <p><?php echo $_smarty_tpl->tpl_vars['performer']->value['about'];?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['performer']->value['phone'];?>
 <?php echo $_smarty_tpl->tpl_vars['performer']->value['name'];?>
</p>

                    <section class="slideshow">
                        <ul>
                            <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['performer']->value['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
                                <li>
                                    <figure>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" alt=""/>
                                    </figure>
                                </li>
                            <?php } ?>
                        </ul>
                        <nav>
                            <span class="glyphicon glyphicon-chevron-left nav-prev"></span>
                            <span class="glyphicon glyphicon-chevron-right nav-next"></span>
                            <span class="glyphicon glyphicon-remove nav-close"></span>
                        </nav>
                    </section>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<?php }} ?>
