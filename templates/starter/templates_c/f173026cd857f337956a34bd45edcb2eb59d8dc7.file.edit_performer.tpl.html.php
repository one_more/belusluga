<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-26 21:12:54
         compiled from "/var/www/belusluga.my/templates/starter/templates/admin_panel/edit_performer.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:74340137553cdb82a89089-72756742%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f173026cd857f337956a34bd45edcb2eb59d8dc7' => 
    array (
      0 => '/var/www/belusluga.my/templates/starter/templates/admin_panel/edit_performer.tpl.html',
      1 => 1430071974,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '74340137553cdb82a89089-72756742',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_553cdb82bb17f3_22290423',
  'variables' => 
  array (
    'performer' => 0,
    'data' => 0,
    'photos' => 0,
    'name' => 0,
    'phone' => 0,
    'category' => 0,
    'categories' => 0,
    'el' => 0,
    'about' => 0,
    'select_photo' => 0,
    'image' => 0,
    'delete' => 0,
    'save' => 0,
    'cancel' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553cdb82bb17f3_22290423')) {function content_553cdb82bb17f3_22290423($_smarty_tpl) {?><form id="edit-performer-form">
    <input value="<?php echo $_smarty_tpl->tpl_vars['performer']->value['id'];?>
" name="id" type="hidden"/>
    <ul id="tabs" class="nav nav-tabs">
        <li class="active"><a class="cursor-pointer" data-param="data"><?php echo $_smarty_tpl->tpl_vars['data']->value;?>
</a></li>
        <li><a class="cursor-pointer" data-param="photo"><?php echo $_smarty_tpl->tpl_vars['photos']->value;?>
</a></li>
    </ul>
    <div id="tabs-data">
        <div id="data">
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="name"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</label>
                        <input value="<?php echo $_smarty_tpl->tpl_vars['performer']->value['name'];?>
" id="name" name="name" class="form-control" type="text"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="phone"><?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
</label>
                        <input  value="<?php echo $_smarty_tpl->tpl_vars['performer']->value['phone'];?>
" class="form-control" id="phone" name="phone" type="text"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="category"><?php echo $_smarty_tpl->tpl_vars['category']->value;?>
</label>
                        <select name="category" id="category" class="form-control">
                            <?php  $_smarty_tpl->tpl_vars['el'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['el']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['el']->key => $_smarty_tpl->tpl_vars['el']->value) {
$_smarty_tpl->tpl_vars['el']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['el']->key;
?>
                                <?php if ($_smarty_tpl->tpl_vars['el']->value['id']==$_smarty_tpl->tpl_vars['performer']->value['category']) {?>
                                    <option selected value="<?php echo $_smarty_tpl->tpl_vars['el']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['el']->value['name'];?>
</option>
                                <?php } else { ?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['el']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['el']->value['name'];?>
</option>
                                <?php }?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="about"><?php echo $_smarty_tpl->tpl_vars['about']->value;?>
</label>
                        <textarea class="form-control" id="about" name="about"><?php echo $_smarty_tpl->tpl_vars['performer']->value['about'];?>
</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div id="photo" class="hide margin-top-10px margin-bottom-10px">
            <div class="row">
                <div class="col-lg-4 padding-right-5px">
                    <input name="photos" readonly class="form-control" id="photos-names" type="text"/>
                </div>
                <div class="col-lg-4 padding-0">
                    <button class="btn btn-primary" id="select-photo"><?php echo $_smarty_tpl->tpl_vars['select_photo']->value;?>
</button>
                </div>
                <input name="images" type="file" multiple class="opacity-0"/>
            </div>
            <div class="row">
                <div id="thumbnails">
                    <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['performer']->value['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
                        <div class="thumbnail display-inline-block">
                            <img style="width: 150px; height: 110px;" src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" alt=""/>
                            <div class="caption">
                                <button class="btn btn-danger btn-xs remove-exist-img">
                                    <?php echo $_smarty_tpl->tpl_vars['delete']->value;?>

                                </button>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <input class="btn btn-success" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['save']->value;?>
"/>
                <button id="cancel" class="btn btn-info"><?php echo $_smarty_tpl->tpl_vars['cancel']->value;?>
</button>
            </div>
        </div>
    </div>
</form>
<?php }} ?>
