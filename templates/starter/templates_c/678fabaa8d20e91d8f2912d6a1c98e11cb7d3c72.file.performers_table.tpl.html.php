<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-26 18:30:53
         compiled from "/var/www/belusluga.my/templates/starter/templates/admin_panel/performers_table.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:2133699484553bff93d39f42-90684421%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '678fabaa8d20e91d8f2912d6a1c98e11cb7d3c72' => 
    array (
      0 => '/var/www/belusluga.my/templates/starter/templates/admin_panel/performers_table.tpl.html',
      1 => 1430062244,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2133699484553bff93d39f42-90684421',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_553bff93d5ecc9_82612138',
  'variables' => 
  array (
    'name' => 0,
    'phone' => 0,
    'category' => 0,
    'performers' => 0,
    'performer' => 0,
    'edit' => 0,
    'delete' => 0,
    'add' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553bff93d5ecc9_82612138')) {function content_553bff93d5ecc9_82612138($_smarty_tpl) {?><table id="performers-table" class="table table-striped">
    <thead>
    <th><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</th>
    <th><?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
</th>
    <th><?php echo $_smarty_tpl->tpl_vars['category']->value;?>
</th>
    <th></th>
    <th></th>
    </thead>
    <tbody>
        <?php  $_smarty_tpl->tpl_vars['performer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['performer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['performers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['performer']->key => $_smarty_tpl->tpl_vars['performer']->value) {
$_smarty_tpl->tpl_vars['performer']->_loop = true;
?>
        <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['performer']->value['name'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['performer']->value['phone'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['performer']->value['category'];?>
</td>
            <td width="30"><a href="/admin_panel/edit_performer/<?php echo $_smarty_tpl->tpl_vars['performer']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['edit']->value;?>
</a></td>
            <td width="30">
                <a data-param="<?php echo $_smarty_tpl->tpl_vars['performer']->value['id'];?>
" class="cursor-pointer delete-performer">
                    <?php echo $_smarty_tpl->tpl_vars['delete']->value;?>

                </a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<a class="btn btn-primary" href="/admin_panel/add_performer"><?php echo $_smarty_tpl->tpl_vars['add']->value;?>
</a><?php }} ?>
