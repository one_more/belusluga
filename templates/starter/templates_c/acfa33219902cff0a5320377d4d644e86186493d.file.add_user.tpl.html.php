<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-26 13:44:58
         compiled from "/var/www/belusluga.my/templates/starter/templates/admin_panel/add_user.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:130884862553cc1aaf1dc99-78931253%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'acfa33219902cff0a5320377d4d644e86186493d' => 
    array (
      0 => '/var/www/belusluga.my/templates/starter/templates/admin_panel/add_user.tpl.html',
      1 => 1429954312,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '130884862553cc1aaf1dc99-78931253',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'login' => 0,
    'password' => 0,
    'credentials' => 0,
    'credentials_labels' => 0,
    'k' => 0,
    'el' => 0,
    'add' => 0,
    'cancel' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_553cc1aaf39ed6_42113836',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553cc1aaf39ed6_42113836')) {function content_553cc1aaf39ed6_42113836($_smarty_tpl) {?>
<form id="add-user-form">
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <label for="login"><?php echo $_smarty_tpl->tpl_vars['login']->value;?>
</label>
                <input id="login" name="login" class="form-control" type="text"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <label for="password"><?php echo $_smarty_tpl->tpl_vars['password']->value;?>
</label>
                <input class="form-control" id="password" name="password" type="text"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <label for="credentials"><?php echo $_smarty_tpl->tpl_vars['credentials']->value;?>
</label>
                <select name="credentials" id="credentials" class="form-control">
                    <?php  $_smarty_tpl->tpl_vars['el'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['el']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['credentials_labels']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['el']->key => $_smarty_tpl->tpl_vars['el']->value) {
$_smarty_tpl->tpl_vars['el']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['el']->key;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['el']->value;?>
</option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <input class="btn btn-success" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['add']->value;?>
"/>
                <button class="btn btn-info"><?php echo $_smarty_tpl->tpl_vars['cancel']->value;?>
</button>
            </div>
        </div>
    </div>
</form>
<?php }} ?>
