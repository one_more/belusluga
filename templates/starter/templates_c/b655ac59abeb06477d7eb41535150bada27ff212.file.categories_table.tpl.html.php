<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-27 11:55:38
         compiled from "/var/www/belusluga.my/templates/starter/templates/admin_panel/categories_table.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:1710560825553d5fbce0bc46-55406973%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b655ac59abeb06477d7eb41535150bada27ff212' => 
    array (
      0 => '/var/www/belusluga.my/templates/starter/templates/admin_panel/categories_table.tpl.html',
      1 => 1430124937,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1710560825553d5fbce0bc46-55406973',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_553d5fbce449c3_90501588',
  'variables' => 
  array (
    'name' => 0,
    'categories' => 0,
    'category' => 0,
    'edit' => 0,
    'delete' => 0,
    'add' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553d5fbce449c3_90501588')) {function content_553d5fbce449c3_90501588($_smarty_tpl) {?><table id="categories-table" class="table table-striped">
    <thead>
    <th><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</th>
    <th></th>
    <th></th>
    </thead>
    <tbody>
        <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
        <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['category']->value['name'];?>
</td>
            <td width="30"><a href="/admin_panel/edit_category/<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['edit']->value;?>
</a></td>
            <td width="30">
                <a data-param="<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
" class="cursor-pointer delete-category">
                    <?php echo $_smarty_tpl->tpl_vars['delete']->value;?>

                </a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<a class="btn btn-primary" href="/admin_panel/add_category"><?php echo $_smarty_tpl->tpl_vars['add']->value;?>
</a><?php }} ?>
