<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-25 22:53:04
         compiled from "/var/www/belusluga.my/templates/starter/templates/admin_panel/index.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:1289206220553bf0a0a97c45-26569582%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '78930da1cd18fb41f6b38615e0c0c5820a91e255' => 
    array (
      0 => '/var/www/belusluga.my/templates/starter/templates/admin_panel/index.tpl.html',
      1 => 1429954312,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1289206220553bf0a0a97c45-26569582',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'css_path' => 0,
    'images_path' => 0,
    'navbar' => 0,
    'left_menu' => 0,
    'main_content' => 0,
    'js_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_553bf0a0ab9a32_98857255',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553bf0a0ab9a32_98857255')) {function content_553bf0a0ab9a32_98857255($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
    <title>Admin panel</title>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['css_path']->value;?>
/main.css"/>
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/favicon.ico"/>
</head>
<body>
    <?php echo $_smarty_tpl->tpl_vars['navbar']->value;?>

    <div class="container-fluid">
        <div class="row">
            <div id="left_menu" class="col-lg-2 col-lg-offset-1">
                <?php echo $_smarty_tpl->tpl_vars['left_menu']->value;?>

            </div>
            <div id="main_content" class="col-lg-7">
                <?php echo $_smarty_tpl->tpl_vars['main_content']->value;?>

            </div>
        </div>
    </div>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
/main.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
/admin_panel_views.js"><?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
