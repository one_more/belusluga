<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-05-01 01:20:01
         compiled from "/var/www/belusluga.my/templates/starter/templates/index/index.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:1352422162553ba051ef1e85-53997428%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '40f2c328d0ef2ccf3692a1a0a9d0a962cef469a3' => 
    array (
      0 => '/var/www/belusluga.my/templates/starter/templates/index/index.tpl.html',
      1 => 1430432401,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1352422162553ba051ef1e85-53997428',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_553ba052018998_29620678',
  'variables' => 
  array (
    'css_path' => 0,
    'images_path' => 0,
    'top_menu' => 0,
    'page_text' => 0,
    'main_content' => 0,
    'js_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553ba052018998_29620678')) {function content_553ba052018998_29620678($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
    <title>Belusluga</title>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['css_path']->value;?>
/main.css"/>
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/favicon.ico"/>
</head>
<body id="body">
    <div id="all" class="container-fluid">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <header id="header">
                    <div class="row">
                        <div class="col-lg-2">
                            <div id="logo">Belusluga</div>
                        </div>
                        <div class="col-lg-10">
                            <?php echo $_smarty_tpl->tpl_vars['top_menu']->value;?>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div id="describe">
                    <?php echo $_smarty_tpl->tpl_vars['page_text']->value;?>

                </div>
            </div>
        </div>
        <div class="row">
            <div class=" col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1 col-xs-9 col-xs-offset-2">
                <div id="main_content">
                    <?php echo $_smarty_tpl->tpl_vars['main_content']->value;?>

                </div>
            </div>
        </div>
        <footer id="footer" class="col-lg-8 col-lg-offset-2">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div id="footer-content">&nbsp;</div>
                </div>
            </div>
        </footer>
    </div>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
/main.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
/site_views.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/ck_editor/ckeditor.js"><?php echo '</script'; ?>
>
</body>
</html><?php }} ?>
