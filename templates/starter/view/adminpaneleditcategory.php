<?php

class AdminPanelEditCategory extends TemplateView {

	public function __construct($id) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'admin_panel');
		$controller = Application::get_class('CategoriesController');
		$this->assign('category', $controller->get_category($id));
	}

	public function render() {
		return $this->getTemplate('edit_category.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'admin_panel_edit_category.json';
	}
}