<?php

class AdminPanelCategoriesTable extends TemplateView {
	public function __construct() {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'admin_panel');
	}

	public function render() {
		$controller = Application::get_class('CategoriesController');
		$this->assign('categories', $controller->get_categories());
		return $this->getTemplate('categories_table.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'admin_panel_categories_table.json';
	}
}