<?php

class Text extends TemplateView {
	private $section;

	public function __construct($section) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'text');
		$this->section = $section;
	}

	public function render() {
		$controller = Application::get_class('TextController');
		$this->assign('text', $controller->get_section($this->section));
		$save_url = "/update_text?section={$this->section}";
		$this->assign('save_url', $save_url);
		$user_controller = Application::get_class('UserController');
		$this->assign('is_admin', $user_controller->is_admin());
		return $this->getTemplate('text.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'text.json';
	}
}