<?php

class AdminPanelPerformersTable extends TemplateView {
	public function __construct() {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'admin_panel');
	}

	public function render() {
		$controller = Application::get_class('PerformersController');
		$this->assign('performers', $controller->get_performers());
		return $this->getTemplate('performers_table.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'admin_panel_performers_table.json';
	}
}