<?php

class MainMenu extends TemplateView {
	public function __construct() {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'menu');
	}

	public function render() {
		$active_item = strpos(Request::uri(), 'goods') !== false ? 'goods' : 'services';
		$this->assign('active_item', $active_item);
		return $this->getTemplate('main_menu.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'main_menu.json';
	}
}