<?php

class AdminPanelEditPerformer extends TemplateView {

	public function __construct($id) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'admin_panel');
		$controller = Application::get_class('PerformersController');
		$this->assign('performer', $controller->get_performer($id));
	}

	public function render() {
		$controller = Application::get_class('CategoriesController');
		$this->assign('categories', $controller->get_categories());
		return $this->getTemplate('edit_performer.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'admin_panel_edit_performer.json';
	}
}