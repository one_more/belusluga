(function() {
    window.ServicesView = Backbone.View.extend({
        el: '#performers',

        initialize: function() {
            var $this = this;
            App.on('Page:loaded', function() {
                if($('#performers').length) {
                    $this.setElement('#performers');
                    $this.init_sliders();
                }
            });
            $this.init_sliders();
        },

        events: {
            'click .performer .preview': 'show_info',
            'click .performer .hide-info': 'hide_info',
            'click .performer .prev-slide': 'prev_slide',
            'click .performer .next-slide': 'next_slide'
        },

        show_info: function(event) {
            var $this = ServicesView;
            var el = $(event.target).is('.performer') ? $(event.target) :
                $(event.target).parents('.performer');
            var info = el.find('.performer-info');
            if(info.hasClass('hide')) {
                $this.$el.find('.performer-info').each(function(i,el) {
                    if(!$(el).hasClass('hide')) {
                        $(el).find('.hide-info').trigger('click');
                    }
                });
                el.find('.category').hide();
                info.css('display', 'none').removeClass('hide').slideDown('slow');
            }
        },

        hide_info: function(event) {
            event.stopPropagation();
            var el = $(event.target).is('.performer') ? $(event.target) :
                $(event.target).parents('.performer');
            el.find('.performer-info').slideUp('slow', function() {
                $(this).addClass('hide');
                el.find('.category').show();
            });

        },

        init_sliders: function() {
            $('.performer').each(function(k, el) {
                new Swiper($(el).find('.performer-images').get(0), {
                    loop: true,
                    grabCursor: true
                }).on('slideChangeEnd', function(swiper) {
                        var index = swiper.activeIndex;
                        var slides_count = swiper.slides.length-2;
                        if(index == 0) {
                            index = slides_count;
                        }
                        if(index > slides_count) {
                            index = 1;
                        }
                        $(swiper.container).parents('.performer').find('.slides-active-index')
                            .text(index);
                });

                new CBPGridGallery(el);
            });
        },

        prev_slide: function(event) {
            var el = $(event.target);
            var swiper = el.parents('.performer').find('.performer-images').get(0).swiper;
            swiper.slidePrev();
        },

        next_slide: function(event) {
            var el = $(event.target);
            var swiper = el.parents('.performer').find('.performer-images').get(0).swiper;
            swiper.slideNext();
        }
    });
    window.ServicesView = new window.ServicesView;
})();
