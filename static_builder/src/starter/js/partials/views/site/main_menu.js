(function() {
    'use strict';
    window.MainMenuView = Backbone.View.extend({
        el: '#main-menu',

        events: {
            'click a': 'change_active'
        },

        change_active: function(event) {
            var $this = MainMenuView;
            var el = $(event.target);
            $this.$el.find('a').removeClass('active');
            el.addClass('active');
        }
    });
    window.MainMenuView = new window.MainMenuView;
})();
