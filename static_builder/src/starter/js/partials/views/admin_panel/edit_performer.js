(function() {
    'use strict';
    window.EditPerformerView = $.extend(true, window.PerformerView, {
        el: '#edit-performer-form',

        images_to_delete: [],

        initialize: function() {
            var $this = this;
            App.on('Page:loaded', function() {
                if(location.pathname.indexOf('admin_panel/edit_performer') !== -1) {
                    $this.setElement('#edit-performer-form');
                    $this.images_to_delete = [];
                }
            })
        },

        events: {
            "click .remove-exist-img": "remove_exist_image"
        },

        remove_exist_image: function(event) {
            var $this = window.EditPerformerView;
            var el = $(event.target);
            var img = el.parents('.thumbnail').find('img').attr('src').
                split('/').slice(-1)[0];
            $this.images_to_delete.push(img);
            el.parents('.thumbnail').remove();
        },

        save: function(event) {
            var $this = EditPerformerView;
            var table = event.target;
            var id = $this.$el.find('[name=id]').val();
            $.post('/edit_performer', $(table).serializeArray(), function(json) {
                if(json.status == 'success') {
                    if($this.images_to_delete.length && $this.files_queue.length) {
                        $.when(
                                App.upload_images($this.files_queue, '/add_performer_images/'+id),
                                $.post('/delete_performer_images', {
                                    id: id,
                                    images: $this.images_to_delete
                                })
                            )
                            .done(function() {
                                $this.after_save(json);
                            });
                    } else if($this.files_queue.length) {
                        App.upload_images($this.files_queue, '/add_performer_images/'+id)
                            .then(function() {
                                $this.after_save(json);
                            })
                    } else if($this.images_to_delete.length) {
                        $.post('/delete_performer_images', {
                            id: id,
                            images: $this.images_to_delete
                        }, function() {
                            $this.after_save(json);
                        })
                    } else {
                        $this.after_save(json);
                    }
                } else {
                    NotificationView.display(json.message, json.status);
                }
            }, 'json');
        },

        after_save: function(json) {
            NotificationView.display(json.message, json.status);
            App.go_to('/admin_panel/performers');
            this.files_queue = [];
            this.images_to_delete = [];
        }
    });
    for(var i in EditPerformerView) {
        if(typeof EditPerformerView[i] == 'function' && PerformerView[i]) {
            EditPerformerView[i].bind(EditPerformerView);
        }
    }
    window.EditPerformerView = Backbone.View.extend(EditPerformerView);
    window.EditPerformerView = new window.EditPerformerView;
})();

