(function() {
    window.EditCategoryView = Backbone.View.extend({
        el: '#edit-category-form',

        initialize: function() {
            var $this = this;
            App.on('Page:loaded', function() {
                if(location.pathname.indexOf('/admin_panel/edit_category') !== -1) {
                    $this.setElement('#edit-category-form');
                }
            })
        },

        events: {
            'submit': 'save',
            'click button': 'cancel'
        },

        save: function(event) {
            var form = $(event.target);
            $.post('/edit_category', form.serializeArray(), function(json) {
                if(json.status == 'success') {
                    App.go_to('/admin_panel/categories')
                }
                NotificationView.display(json.message, json.status);
            }, 'json')
        },

        cancel: function(event) {
            event.preventDefault();
            App.go_to('/admin_panel/categories');
        }
    });
    window.EditCategoryView = new window.EditCategoryView;
})();
