(function() {
    window.AddCategoryView = Backbone.View.extend({
        el: '#add-category-form',

        initialize: function() {
            var $this = this;
            App.on('Page:loaded', function(event) {
                if(event.page == 'add_category') {
                    $this.setElement('#add-category-form');
                }
            })
        },

        events: {
            'submit': 'save',
            'click button': 'cancel'
        },

        save: function(event) {
            var form = $(event.target);
            $.post('/add_category', form.serializeArray(), function(json) {
                if(json.status == 'success') {
                    App.go_to('/admin_panel/categories')
                }
                NotificationView.display(json.message, json.status);
            }, 'json')
        },

        cancel: function(event) {
            event.preventDefault();
            App.go_to('/admin_panel/categories');
        }
    });
    window.AddCategoryView = new window.AddCategoryView;
})();
