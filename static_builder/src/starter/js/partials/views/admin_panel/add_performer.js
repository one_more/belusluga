(function() {
    'use strict';
    window.AddPerformerView = _.extend(window.PerformerView, {
        el: '#add-performer-form',

        initialize: function() {
            var $this = this;
            App.on('Page:loaded', function(event) {
                if(event.page == 'add_performer') {
                    $this.setElement('#add-performer-form');
                }
            })
        },

        save: function(event) {
            var $this = AddPerformerView;
            var table = event.target;
            $.post('/add_performer', $(table).serializeArray(), function(json) {
                if(json.status == 'success') {
                    App.upload_images($this.files_queue, '/add_performer_images/'+json.id)
                        .then(function() {
                            NotificationView.display(json.message, json.status);
                            App.go_to('/admin_panel/performers');
                            $this.files_queue = [];
                        });
                } else {
                    NotificationView.display(json.message, json.status);
                }
            }, 'json');
        }
    });
    for(var i in AddPerformerView) {
        if(typeof AddPerformerView[i] == 'function' && PerformerView[i]) {
            AddPerformerView[i].bind(AddPerformerView);
        }
    }
    window.AddPerformerView = Backbone.View.extend(AddPerformerView);
    window.AddPerformerView = new window.AddPerformerView;
})();

