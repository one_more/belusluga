(function() {
    'use strict';
    window.PerformersTableView = Backbone.View.extend({
        el: "#performers-table",

        initialize: function() {
            var $this = this;
            App.on('Page:loaded', function(event) {
                if(event.page == 'performers') {
                    $this.setElement('#performers-table');
                }
            })
        },

        events: {
            'click .delete-performer': 'delete_performer'
        },

        delete_performer: function(event) {
            if(confirm(LanguageModel.get('confirm_delete_performer'))) {
                var el = $(event.target);
                $.post('/delete_performer/'+el.data('param'), null, function(json) {
                    if(json.status == 'success') {
                        el.parents('tr').remove();
                    }
                    NotificationView.display(json.message, json.status);
                }, 'json');
            }
        }
    });
    window.PerformersTableView = new window.PerformersTableView;
})();
