(function() {
    'use strict';
    window.CategoriesTableView = Backbone.View.extend({
        el: "#categories-table",

        initialize: function() {
            var $this = this;
            App.on('Page:loaded', function(event) {
                if(event.page == 'categories') {
                    $this.setElement('#categories-table');
                }
            })
        },

        events: {
            'click .delete-category': 'delete_category'
        },

        delete_category: function(event) {
            if(confirm(LanguageModel.get('confirm_delete_category'))) {
                var el = $(event.target);
                $.post('/delete_category/'+el.data('param'), null, function(json) {
                    if(json.status == 'success') {
                        el.parents('tr').remove();
                    }
                    NotificationView.display(json.message, json.status);
                }, 'json');
            }
        }
    });
    window.CategoriesTableView = new window.CategoriesTableView;
})();
